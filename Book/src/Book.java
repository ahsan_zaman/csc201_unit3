
public class Book {
	private String Title;
	private String Author;
	private int numberOfPages;
	
	public Book(){														// Default constructor
		Title = null;
		Author = null;
		numberOfPages = 0;
	}
	
	public Book( String Title, String Author, int numberOfPages ){		
		this.Title = Title;
		this.Author = Author;
		this.numberOfPages = numberOfPages;
	}
	
	public String toString(){											// Returns a string with the information on book
		return "Title: "+Title+"\nAuthor: "+Author+"\nNumber of Pages: "+numberOfPages;
	}
}
