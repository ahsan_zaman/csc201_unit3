
public class Volume {
	private String volumeName;
	private int numberOfBooks;
	private Book[] objects;
	
	public Volume(){																// Default constructor
		volumeName = null;
		numberOfBooks = 0;
	}
	
	public Volume( String volumeName, int numberOfBooks, Book[] objects ){			// Constructor	
		this.volumeName = volumeName;
		this.numberOfBooks = numberOfBooks;
		this.objects = objects;
	}
	
	public String toString(){														// Returns a string with the information about the volume
		return "Volume Name: "+volumeName+"\nNumber of Books: "+numberOfBooks;
	}
	
	public void displayBooks(){														// Displaying all books in the volume
		for( int i=0; i<5 ;i++ ){
			System.out.println( objects[i].toString() );
		}
	}
	
	public Book[] getBookArray(){													// A getter method
		return objects;
	}
}
