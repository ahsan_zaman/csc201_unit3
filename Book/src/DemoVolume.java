/**
 * @author Ahsan Zaman
 * CSC 201 - Unit 3
 * Book 
 * 
 * Algorithm:
 * 1. Declare and initialize Scanner object input
 * 2. Create an array of 5 Book objects
 * 3. Declare String variables Title and Author
 * 4. Declare and Initialize integer variable pages 
 * 5. Run for loop 5 times
 * 		6. Take input string for Title
 * 		7. Take input string for author
 * 		8. Take input for pages
 * 		9. Pass Title, Author, pages into constructor for book object at index i
 * 10. Create and initialize volume object with a string, 5 and book array of objects
 * 11. Display volume information using toString
 * 12. Close Scanner object
 * END
 */
import java.util.Scanner;
public class DemoVolume {
	public static void main( String[] args ){
		Scanner input = new Scanner(System.in);
		Book[] bk = new Book[5];
		String Title, Author;
		int pages=0;
		for( int i=0; i<5 ;i++ ){					  						 // Taking input for the array of Book objects
			System.out.println( "Please enter information for book #"+(i+1)+"\nTitle: " );
			Title = input.nextLine();
			System.out.println( "Author: " );
			Author = input.nextLine();
			System.out.println( "Number of Pages: " );
			pages = input.nextInt();
			input.nextLine();
			bk[i] = new Book( Title, Author, pages );
		}
																		
		Volume volume1 = new Volume( "Potter Series", 5, bk );			 	// Creating the volume
		System.out.println( "\nVolume information: "+volume1.toString() ); 	// Displaying volume information	
		volume1.displayBooks();												// Displaying all books in volume1
		
		input.close();
	}
}
