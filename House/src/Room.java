/**
 * @author Ahsan Zaman
 * CSC 201 - Unit 3
 * 
 */
public class Room {
	private String color;
	private String floor;
	private int numofWindows;
	
	public Room(){
		color = null;
		floor = null;
		numofWindows = -1;					// sentinel value 
	}
	
	public Room( String color1, String floor1, int num ){
		color = color1;
		floor = floor1;
		numofWindows = num;
	}
	
	public void displayRoom(){
		System.out.println( "Color of Walls: "+color+"\nFloor: "+floor+"\nNumber of Windows: "+numofWindows );
	}
}
