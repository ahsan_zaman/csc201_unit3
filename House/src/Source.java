/**
 * @author Ahsan Zaman
 * CSC 201 - Unit 3
 * Room 
 * 
 * Algorithm:
 * 1. Declare and Initialize integer variables num and windows.
 * 2. Ask user how many rooms to create.
 * 3. Store user's input in num
 * 4. Declare array of Room objects with num
 * 5. Declare and initialize color1, and floor1.
 * 6. Run a for loop from 0 till num
 * 		7. Take input for color1
 * 		8. Take input for floor1
 * 		9. Take input for windows
 * 		10. Pass color1, floor1, windows into object[i] constructor
 * 	11. Run for loop from 0 till num
 * 		12. Display all rooms
 */
import java.util.Scanner;
public class Source {
	public static Scanner input = new Scanner(System.in);
	
	public static void main( String[] args ){
		int num=0, windows=0;
		System.out.println( "\t\t**Rooms**\nHow many rooms do you wish to create: " );
		num = input.nextInt();											
		Room[] objects = new Room[num];									// Declaring array of objects
		String color1, floor1;
		/**color1 stores user's input for the color of the walls.
		 * floor1 stores user's input for the floor type */
		for( int i=0; i<num ;i++ ){
			System.out.println( "Enter the color of walls for room #"+(i+1) );
			color1 = input.nextLine();
			input.next();
			System.out.println( "Enter the type of floor (hardwood, carpetted or tiled): " );
			floor1 = input.nextLine();
			input.next();
			System.out.println( "Enter the number of windows in the room: " );
			windows = input.nextInt();
			objects[i] = new Room( color1, floor1, windows );			// Passing values through the methods to the private data members
		}
		
		for( int i=0; i<num ;i++ ){										// Displaying entry
			System.out.println( "\n\tRoom #"+(i+1) );
			objects[i].displayRoom();
		}
	}
}
