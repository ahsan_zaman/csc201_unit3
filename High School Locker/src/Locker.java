/**
 * @author Ahsan Zaman
 * Locker class
 */
import java.util.Scanner;
public class Locker {
	
	private CombinationLock lock;
	private int lockerNum;
	private String studentName;
	private int numOfBooks;
	
	public Locker(){
		lockerNum = 0;
		studentName=null;
		numOfBooks=0;
		lock = new CombinationLock();
	}
	
	public Locker( int locker, int first, int second, int third, String name, int books ){
		lock = new CombinationLock( first, second, third );
		studentName = name;
		numOfBooks = books;
		lockerNum = locker;
	}
	
	public void displayLocker(){
		System.out.println( "Locker Number: "+lockerNum );
		System.out.println( "Student Name: "+studentName );
		System.out.println( "Number of Books: "+numOfBooks );
	}
	
	public void putBookInLocker(){				// Increments the books placed in the locker
		numOfBooks++;
	}
	
	public boolean removeBookFromLocker(){
		if( numOfBooks>0 ){						// Checks if there are any number of books in the locker to remove
			numOfBooks--;
			return true;
		}
		return false;
	}
	
	public void resetDial(){					// Resetting dial to 0
		lock.resetDial();
	}
	
	public boolean openLocker(){				// Prompts user to give three inputs
		System.out.println( "Enter three numbers between 0-39 to open locker: " );
		Scanner input = new Scanner(System.in);
		int temp1=-1, temp2=-1, temp3=-1;
		while( temp1<0 | temp1>39 ){			// Ensuring that the input is between 0-39
			temp1 = input.nextInt();
			if( temp1<0 | temp1>39 )
				System.out.println( "Incorrect Number! Number should be from 0 to 39. " );
		}
		while( temp2<0 | temp2>39 ){
			temp2 = input.nextInt();
			if( temp2<0 | temp2>39 )
				System.out.println( "Incorrect Number! Number should be from 0 to 39. " );
		} 
		while( temp3<0 | temp3>39 ){
			temp3 = input.nextInt();
			if( temp3<0 | temp3>39 )
				System.out.println( "Incorrect Number! Number should be from 0 to 39. " );
		}
		if( lock.openLock(temp1, temp2, temp3 )==true ){
			System.out.println( "Your locker has been successfull opened! " );
			return true;
		}
		else {
			System.out.println( "The combination was incorrect. " );
			return false;
		}
	}
	
	public int getLockerNum(){					// lockerNum getter
		return lockerNum;
	}
}
