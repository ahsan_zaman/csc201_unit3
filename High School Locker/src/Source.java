/**
 * @author Ahsan Zaman
 * CSC 201 - Unit 3
 * High School Locker
 * 
 * Algorithm:
 * 1. Declare Scanner object input.
 * 2. Declare and initialize Locker object lk1, lk2 to the given values.
 * 3. Declare and initialize integer variables choice and locker
 * 4. While choice is NOT equals to 0.
 * 		5. Take user input for locker number in locker
 * 		6. Compare locker with each locker number to find the specified locker
 * 		7. When locker is found, ask user to input the combination to open the locker
 * 		8. When the combination is correct and locker has been opened, display menu to user.
 * 		9. Use a switch case to match the appropriate choice that the user chose
 * 10 Exit.
 * END
 */
import java.util.Scanner;
public class Source {
	
	public enum Menu{
		EXIT_SEQUENCE, DISPLAY_LOCKER, ADD_BOOK, REMOVE_BOOK; 
	}
	
	public static void main( String[] args ){
																		// Declaring and initializing variables
		Scanner input = new Scanner(System.in);
		Locker lk1 = new Locker( 100, 28, 17, 39, "Mickey", 3 );		// Lockers
		Locker lk2 = new Locker( 275, 35, 16, 27, "Donald Duck", 0 );
		int choice=-1, locker=0;						
		/** locker stores the locker number user wants to find. Choice is -1 because it is a sentinel value not being used.*/
		
		while( choice!=0 ){									
			System.out.println( "Enter locker number: " );				// Search by locker number
			locker = input.nextInt();
			
			if( locker == lk1.getLockerNum() ){							// if matches locker number
				System.out.println( "Enter locker combination to Open locker: " );
				
				if( lk1.openLocker()==true ){							// If combination is good
					System.out.println( "Which action do you wish to perform: \n1. Display locker" );
					System.out.println( "2. Add book to locker\n3. Remove book from locker\nor press 0 to exit." );
					choice = input.nextInt();
					
					switch( Menu.values()[choice] ){					// Actions to perform in the locker
					case DISPLAY_LOCKER:
						lk1.displayLocker();
						break;
					case ADD_BOOK:
						lk1.putBookInLocker();
						break;
					case REMOVE_BOOK:
						if( lk1.removeBookFromLocker() )
							System.out.println( "Book removed. " );
						else System.out.println( "No book to remove. " );
						break;
					case EXIT_SEQUENCE:
						System.out.println( "Exit sequence. " );
					}
				}
			}
			else if( locker == lk2.getLockerNum() ){					// To compare the locker number
				System.out.println( "Enter locker combination to Open locker: " );
				
				if( lk2.openLocker()==true ){
					System.out.println( "Which action do you wish to perform: \n1. Display locker" );
					System.out.println( "2. Add book to locker\n3. Remove book from locker\nor press 0 to exit." );
					choice = input.nextInt();
					
					switch( Menu.values()[choice] ){					// Using choice to find out user's decision
					case EXIT_SEQUENCE:
						System.out.println( "Exit Sequence. " );
						break;
					case DISPLAY_LOCKER:
						lk2.displayLocker();
						break;
					case ADD_BOOK:
						lk2.putBookInLocker();
						break;
					case REMOVE_BOOK:
						if( lk2.removeBookFromLocker() )
							System.out.println( "Book removed. " );
						else System.out.println( "No book to remove. " );
						break;
					}
				}
			}
			else System.out.println( "Locker not found! \n" );
		}
	}
}
