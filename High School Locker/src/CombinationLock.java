/**
 * 
 * @author Ahsan Zaman
 * CombinationLock class
 */

public class CombinationLock {
	
	private int firstCom;
	private int secondCom;
	private int thirdCom;
	public int dialPosition;
	
	public CombinationLock(){
		firstCom = 0;
		secondCom = 0;
		thirdCom = 0;
		dialPosition = 0;
	}
	
	public CombinationLock( int first, int second, int third ){
		firstCom = first;
		secondCom = second;
		thirdCom = third;
	}
	
	public void resetDial(){						// Resets Dial to 0
		dialPosition = 0;
	}
	
	public void turnLeft( int ticks ){				// Turns left by a number of ticks
		if( ticks>dialPosition ){
			ticks-=dialPosition;
			dialPosition = 0;
			dialPosition = 40-ticks;
		}
		else dialPosition-=ticks;
	}
	
	public void turnRight( int ticks ){				// Turns right by a number of ticks
		if( ( ticks+dialPosition )>=40 ){
			dialPosition+=ticks;
			dialPosition-=40;
		}
		else dialPosition+=ticks;
	}
	
	public boolean openLock( int first, int second, int third ){
		resetDial();
		turnRight( first );
		if( dialPosition==firstCom ){
			turnLeft( second );
			if( dialPosition==secondCom ){
				turnRight( third );
				if( dialPosition==thirdCom )		// When the combination is good
					return true;
				else return false;
			}
			else return false;
		}
		else return false;
	}
}
