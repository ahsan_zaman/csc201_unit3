import java.lang.Character;
public class CharacterArray {
	private char[] array;
	private int length;
	
	public CharacterArray( String str ){
		array = new char[str.length()];							// Initializes array with str's length
		for( int i=0; i<str.length() ;i++ ){
			array[i] = str.charAt(i);							// Fills up the array with str string
		}
		length = str.length();
	}
	
	public void convertString(){								// Converts string from uppercase to lower and lower to upper and digits to *
		for( int i=0; i<length ;i++ ){
			if( Character.isDigit( array[i] ) == true )
				array[i] = '*';
			else if( Character.isLetter( array[i] ) == true ){
				if( Character.isLowerCase( array[i] ) == true )
					array[i] = Character.toUpperCase( array[i] );
				else if( Character.isUpperCase( array[i] ) == true )
					array[i] = Character.toLowerCase( array[i] );
			}
		}
	}
	
	public void displayArray(){									// Displaying results
		System.out.println( "Converted String: " );
		for( int i=0; i<length ;i++ ){
			System.out.print( array[i] );
		}
	}
}
