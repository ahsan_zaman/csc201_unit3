/*
 * Ahsan Zaman
 * CSC 201 - Unit 3
 * Character Array
 * 
 * Algorithm:
 * 1. Initialize and declare string variable str.
 * 2. Create a BufferReader object for taking input
 * 3. Ask user to give the string.
 * 4. Declare a CharacterArray object and initialize it with str
 * 5. Call convertString method through object.
 * 6. Call displayArray.
 * END
 */
import java.io.*;
public class Source {
	public static void main( String[] args ) throws IOException{
		String str;
		BufferedReader input = new BufferedReader( new InputStreamReader (System.in) );
		System.out.println( "Input: " );
		str = input.readLine().toString();										// Taking input
		CharacterArray object = new CharacterArray( str );						// Declaring and passing str string as argument into constructor
		object.convertString();													// Converts code to the specified standard
		object.displayArray();
	}
}
