
public class Cycle {
	private int numOfWheels;
	private int weight;
	
	public Cycle( int numOfWheels, int weight ){
		this.numOfWheels = numOfWheels;
		this.weight = weight;
	}
	
	public String toString(){
		return "Number of wheels: "+numOfWheels+"\nWeight: "+weight;
	}
	
	public Cycle(){
		this( 100, 1000 );						// Passing values into overloaded constructor
	}
}
