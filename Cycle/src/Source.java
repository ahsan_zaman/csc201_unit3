/*
 * Ahsan Zaman
 * CSC 201 - Unit 3
 * Cycle
 * 
 * Algorithm:
 * 1. Declare and initialize Cycle object with values 2 and 10
 * 2. Display the cycle Attributes using toString method.
 * 3. Call default constructor through the object.
 * 4. Display using toString method.
 * END
 */

public class Source {
	public static void main( String[] args ){
		Cycle obj=new Cycle( 2, 10 );
		System.out.println( "\t\t**Cycle**\n"+obj.toString() );
		obj = new Cycle();
		System.out.println( "After the default constructor is invoked. \n"+obj.toString() );
	}
}
