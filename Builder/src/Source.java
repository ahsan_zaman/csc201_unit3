/*
 * Ahsan Zaman
 * CSC 201 - Unit 3
 * Builder
 * 
 * Algorithm:
 * 1. Declare and Initialize Scanner object input
 * 2. Declare and Initialize String variable user
 * 3. Ask user to enter first string
 * 4. Take input from user
 * 5. Create a Builder object and Initialize it with variable user
 * 6. Call appendString through buildString and pass " I love it!"
 * 7. Call displayString through buildString
 * 8. Ask user for the second string
 * 9. Take input from user
 * 10. Call insertString and pass user into it
 * 11. Call displayString through buildString 
 * 12. Close Scanner object.
 * END
 */
import java.util.Scanner;
public class Source {
	public static void main( String[] args ){
		Scanner input = new Scanner(System.in);
		String user=null;
		
		System.out.println( "Enter string: " );					// First Input
		user = input.nextLine();
		
		Builder buildString = new Builder( user );				// Creating a builder object and passing user string as argument
		buildString.appendString( " I love it!" );				// Calling append through builder class
		buildString.displayString();
		
		System.out.println( "Enter second string: " );			// Second Input
		user = input.nextLine();
		
		buildString.insertString( " "+user );					// Inserts string at the length of first string's offset
		buildString.displayString();
		
		input.close();
	}
}
