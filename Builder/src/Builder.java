
public class Builder {
	private StringBuilder strBuild;
	private int length;
	
	public Builder( String str ){
		this.strBuild = new StringBuilder( str );			// Initializing stringbuilder class object using the string passed as argument into method
		this.length = str.length();							// Storing this original length
	}
	
	public void appendString( String str ){
		strBuild = strBuild.append(str);					// Appending using stringbuilder class
	}
	
	public void insertString( String str){
		strBuild = strBuild.insert( length, str );			// inserting str with offset at original string's length
	}
	
	public void displayString(){
		System.out.println( strBuild );
	}
}
